import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import proto.Gate;
import proto.GateServiceGrpc;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    private static boolean dateValidation(String date)
    {
        boolean status = false;
        if (checkDate(date)) {
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            dateFormat.setLenient(false);
            try {
                dateFormat.parse(date);
                status = true;
            } catch (Exception e) {
                status = false;
            }
        }
        return status;
    }

    static boolean checkDate(String date) {
        String pattern = "(0?[1-9]|[12][0-9]|3[01])\\/(0?[1-9]|1[0-2])\\/([0-9]{4})";
        boolean flag = false;
        if (date.matches(pattern)) {
            flag = true;
        }
        return flag;
    }

    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8999).usePlaintext().build();
        GateServiceGrpc.GateServiceStub gateStub = GateServiceGrpc.newStub(channel);

        System.out.println("Options:");
        System.out.println("1. Enter date and receive the corresponding zodiac sign.");

        System.out.println("\n0. QUIT\n");

        boolean isConnected = true;
        while (isConnected) {
            Scanner input = new Scanner(System.in);
            System.out.print("R: ");
            int option = -1;
            try {
                option = input.nextInt();
            }
            catch(InputMismatchException e) {
                System.out.println("Optiune gresita.");
            }
            switch (option) {
                case 1: {
                    System.out.print("Introduceti o data calendaristica in format dd/mm/yyyy: ");
                    Scanner sc = new Scanner(System.in);

                    String date = sc.next();

                    while(!dateValidation(date)) {
                        System.out.print("Data introdusa este gresita! Reintroduceti: ");
                        date = sc.next();
                    }

                    gateStub.getZodiacSign(Gate.DateRequest.newBuilder().setDate(date).build(), new StreamObserver<Gate.ZodiacSignResponse>() {
                        @Override
                        public void onNext(Gate.ZodiacSignResponse zodiacResponse) {
                            System.out.println("Zodia specifica datei introduse este: " + zodiacResponse.getZodiacSign());
                            System.out.println("Introduceti noua optiune: ");
                        }

                        @Override
                        public void onError(Throwable throwable) {
                            System.out.println("Error : " + throwable.getMessage());
                        }

                        @Override
                        public void onCompleted() {

                        }
                    });

                    break;
                }
                case 0: {
                    isConnected = false;
                    break;
                }
                default:
                    System.out.println("Please select 1 or 0!");
            }
        }
        channel.shutdown();
    }
}

