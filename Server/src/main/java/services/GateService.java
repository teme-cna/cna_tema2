package services;

import io.grpc.stub.StreamObserver;
import proto.Gate.*;
import proto.GateServiceGrpc;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class GateService extends GateServiceGrpc.GateServiceImplBase {

    private ArrayList<Pair> springZodiacSigns;
    private ArrayList<Pair> summerZodiacSigns;
    private ArrayList<Pair> fallZodiacSigns;
    private ArrayList<Pair> winterZodiacSigns;

    public GateService() {
        springZodiacSigns = readSeasonalZodiacSigns("spring.txt");
        summerZodiacSigns = readSeasonalZodiacSigns("summer.txt");
        fallZodiacSigns = readSeasonalZodiacSigns("fall.txt");
        winterZodiacSigns = readSeasonalZodiacSigns("winter.txt");
    }

    private ArrayList<Pair> readSeasonalZodiacSigns(String filename) {
        ArrayList<Pair> zodiacSigns = new ArrayList<Pair>();
        try {
            File file = new File(filename);
            Scanner sc = new Scanner(file);

            while (sc.hasNextLine()) {
                String sign = sc.next();

                ArrayList<String> dates = new ArrayList<>();
                String start = sc.next();
                String end = sc.next();

                int xDay = Integer.parseInt(start.substring(0, 2));
                int yDay = Integer.parseInt(end.substring(0, 2));
                int xMonth = Integer.parseInt(start.substring(3, 5));
                int yMonth = Integer.parseInt(end.substring(3, 5));

                while(xDay != yDay || xMonth != yMonth) {
                    String data = Integer.toString(xDay) + "/" + Integer.toString(xMonth);
                    xDay++;
                    if(xDay == 32) {
                        xDay = 1;
                        xMonth++;
                        if(xMonth == 13) {
                            xMonth = 1;
                        }
                    }
                    dates.add(data);
                }
                String lastDate = yDay + "/" + yMonth;
                dates.add(lastDate);
                zodiacSigns.add(new Pair(sign, dates));
            }
            sc.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        return zodiacSigns;
    }

    private String findZodiacSign(ArrayList<Pair> seasonsZodiacSigns, String date) {
        String zodiacSign = null;
        for(int index = 0; index < seasonsZodiacSigns.size(); index++) {
            if(seasonsZodiacSigns.get(index).getDates().contains(date)) {
                zodiacSign = seasonsZodiacSigns.get(index).getSign();
                break;
            }
        }
        return zodiacSign;
    }

    @Override
    public void getZodiacSign(DateRequest request, StreamObserver<ZodiacSignResponse> responseObserver) {
        System.out.println("Accessing getZodiacSign() for the date: " + request.getDate() + ".");

        ZodiacSignResponse.Builder response = ZodiacSignResponse.newBuilder();
        ZodiacSignResponse.Builder sign;

        int day;
        int month;

        if(request.getDate().length() == 10) {
            day = Integer.parseInt(request.getDate().substring(0, 2));
            month = Integer.parseInt(request.getDate().substring(3, 5));
        }
        else {
            if(request.getDate().length() == 8)
            {
                day = Integer.parseInt(request.getDate().substring(0, 1));
                month = Integer.parseInt(request.getDate().substring(2, 3));
            }
            else {
                if (request.getDate().substring(0, 2).contains("/")) {
                    day = Integer.parseInt(request.getDate().substring(0, 1));
                    month = Integer.parseInt(request.getDate().substring(2, 4));
                } else {
                    day = Integer.parseInt(request.getDate().substring(0, 2));
                    month = Integer.parseInt(request.getDate().substring(3, 4));
                }
            }
        }
        String date = day + "/" + month;
        String zodiacSign = null;

        if(month >= 3 && month < 6) {
            zodiacSign = findZodiacSign(springZodiacSigns, date);
        }

        if(month >= 6 && month < 9) {
            zodiacSign = findZodiacSign(summerZodiacSigns, date);
        }

        if(month >= 9 && month < 12) {
            zodiacSign = findZodiacSign(fallZodiacSigns, date);
        }

        if(month == 12 || month == 1 || month == 2) {
            zodiacSign = findZodiacSign(winterZodiacSigns, date);
        }

        ZodiacSignResponse requestedZodiacSign = ZodiacSignResponse.newBuilder().setZodiacSign(zodiacSign).build();
        sign = requestedZodiacSign.toBuilder();

        response.setZodiacSign(sign.getZodiacSign());
        responseObserver.onNext(response.build());
        responseObserver.onCompleted();
    }
}
