package services;

import java.util.ArrayList;

public class Pair {
    private String sign;
    private ArrayList<String> dates;

    public Pair(String sign, ArrayList<String> dates) {
        this.sign = sign;
        this.dates = dates;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public ArrayList<String> getDates() {
        return dates;
    }

    public void setDates(ArrayList<String> dates) {
        this.dates = dates;
    }
}
